# INA138 MODULE

The purpose of this page is to explain step by step the realization of a current monitor based on INA138.

The board uses the following components :

 * an INA138
 * an OPA363
 * some passive components

### ELECTRONICS

The schema is made using KICAD.

### ARDUINO

The code is build using ARDUINO IDE 1.8.11.

### BLOG
A description in french here : https://riton-duino.blogspot.com/2020/07/lina138-current-monitor.html

![Screenshot](https://1.bp.blogspot.com/-j9mV--FlfO4/Xv3YNS6eedI/AAAAAAAAFXI/3cPj26oBwXALwsnHb3Y408H9rpcglrQ8gCK4BGAsYHg/s320/ina138-module.jpg)

